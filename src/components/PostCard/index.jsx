import React from "react";
import { useHistory } from "react-router";
import "./postCard.scss";

const PostCard = ({ post }) => {
  const history = useHistory();
  const { title, excerpt, id } = { ...post };
  return (
    <div className="post-card">
      <div className="post-card__img" />
      <div className="post-card__content">
        <h2 dangerouslySetInnerHTML={{ __html: title?.rendered }} />
        <p dangerouslySetInnerHTML={{ __html: excerpt?.rendered }} />
        <button onClick={() => history.push(`/${id}`, post)}>Read More</button>
      </div>
    </div>
  );
};

export default PostCard;
