import React from "react";
import logo from "../../assets/svg/logo.svg";

import "./header.scss";

const Header = () => {
  return (
    <header className="header">
      <img src={logo} className="header__logo" />
    </header>
  );
};

export default Header;
