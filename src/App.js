import React from "react";
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Header from "./components/Header";
import Posts from "./screens/Posts";
import SinglePost from "./screens/SinglePost";

import "./App.scss";
import Footer from "./components/Footer";

const App = () => {
  return (
    <Router>
      <Header />
      <div className="main">
        <Switch>
          <Route path="/" component={Posts} exact />
          <Route path="/:id" component={SinglePost} exact />
        </Switch>
      </div>
      <Footer />
    </Router>
  );
};

export default App;
