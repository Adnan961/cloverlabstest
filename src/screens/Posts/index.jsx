import React, { useCallback, useEffect, useState } from "react";
import Loader from "../../components/Loader";
import PostCard from "../../components/PostCard";
import "./posts.scss";

const Posts = () => {
  const [posts, setPosts] = useState([]);
  const [loading, setLoading] = useState(true);

  const fetchPosts = useCallback(async () => {
    const respose = await fetch(
      "https://cloverlabs.io/wp-json/wp/v2/posts"
    ).then((res) => res.json());
    if (respose.length) {
      setLoading(false);
      setPosts(respose);
    }
  }, []);

  useEffect(() => {
    fetchPosts();
  }, []);

  return (
    <div className={`post-list ${loading ? "post-list--middle" : ""}`}>
      {loading ? (
        <Loader />
      ) : (
        posts?.map((post) => <PostCard key={post?.id} post={post} />)
      )}
    </div>
  );
};

export default Posts;
