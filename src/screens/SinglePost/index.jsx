import React, { useEffect } from "react";
import "./singlePost.scss";

const SinglePost = ({ location, history }) => {
  const { id, content, title } = { ...location?.state };

  useEffect(() => {
    if (!id) history.push("/");
    window.scrollTo(0, 0);
  }, []);

  return (
    <div className="single-post">
      <div className="single-post__card">
        <div className="single-post__card-img" />
        <h1 dangerouslySetInnerHTML={{ __html: title?.rendered }} />
      </div>
      <p dangerouslySetInnerHTML={{ __html: content?.rendered }} />
    </div>
  );
};

export default SinglePost;
